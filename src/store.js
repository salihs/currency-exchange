import Vue from 'vue'
import Vuex from 'vuex'

import request from './utils/request'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    base: '',
    currencies: []
  },
  mutations: {
    setCurrencies (state, payload) {
      state.currencies = payload.rates
      state.base = payload.base
    }
  },

  actions: {
    getCurrencies ({commit}, payload) {
      request.get('latest')
        .then(response => {
          commit('setCurrencies', response.data)
        })
    }
  },

  getters: {
    getCurrency: (state) => (key) => {
      return state.currencies[key]
    }
  }

})
