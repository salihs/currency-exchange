import axios from 'axios'

const request = axios.create({
  baseURL: 'https://api.exchangeratesapi.io/',
  timeout: 1000,
})

export default request
